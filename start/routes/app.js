'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('v1/app/release', 'app/ReleaseController.index')

Route.get('v1/app/testauth', 'app/TestController.auth').middleware('auth')
Route.get('v1/app/testnoauth', 'app/TestController.noauth')

Route.get('v1/app/mundos', 'app/MundoController.index')
Route.get('v1/app/jogos', 'app/JogoController.index')
Route.get('v1/app/jogoitems', 'app/JogoController.items')
Route.get('v1/app/escolas', 'app/EscolaController.index')
Route.get('v1/app/cenarios', 'app/CenarioController.index')

Route.post('v1/app/registro', 'app/RegistroController.index')
Route.get('v1/app/scores', 'app/ScoreController.index').middleware('auth')
Route.post('v1/app/scores', 'app/ScoreController.save').middleware('auth')
    
/*
Route.group(() => {

    Route.get('testauth', 'TestController.auth')
    Route.get('testnoauth', 'TestController.noauth')

}).prefix('v1/app').namespace('App')
.middleware(['auth'])
*/
