'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
    /*
    Route.resource('users', 'UserController').apiOnly()
        .validator(new Map([
            [['users.store'], ['Admin/StoreUser']],
            [['users.update'], ['Admin/StoreUser']]
        ]))*/
    
    Route.get('posts', 'PostController.index')
    Route.get('posts/:titulo', 'PostController.show')
    Route.get('postssuggestions/:id', 'PostController.suggestions')

    Route.get('tags', 'TagController.index')

}).prefix('v1').namespace('Client')
