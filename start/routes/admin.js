'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {

    Route.post('image/:local/:id/:image_field', 'ImageController.store')

    Route.get('escolas', 'EscolaController.index')
    Route.get('escolas/:id', 'EscolaController.show')
    Route.post('escolas', 'EscolaController.save').validator('Admin/StoreEscola')
    Route.put('escolas/:id', 'EscolaController.update').validator('Admin/UpdateEscola')
    Route.delete('escolas/:id', 'EscolaController.delete')

    Route.get('professores', 'ProfessorController.index')
    Route.get('professores/:id', 'ProfessorController.show')
    Route.post('professores', 'ProfessorController.save').validator('Admin/StoreProfessor')
    Route.put('professores/:id', 'ProfessorController.update').validator('Admin/UpdateProfessor')
    Route.delete('professores/:id', 'ProfessorController.delete')

    Route.get('alunos', 'AlunoController.index')
    Route.get('alunos/:id', 'AlunoController.show')
    Route.post('alunos', 'AlunoController.save').validator('Admin/StoreAluno')
    Route.put('alunos/:id', 'AlunoController.update').validator('Admin/UpdateAluno')
    Route.delete('alunos/:id', 'AlunoController.delete')

    Route.get('cenarios', 'CenarioController.index')
    Route.get('cenarios/:id', 'CenarioController.show')
    Route.post('cenarios', 'CenarioController.save').validator('Admin/StoreCenario')
    Route.put('cenarios/:id', 'CenarioController.update').validator('Admin/StoreCenario')
    Route.delete('cenarios/:id', 'CenarioController.delete')

    Route.get('mundos', 'MundoController.index')
    Route.get('mundos/:id', 'MundoController.show')
    Route.post('mundos', 'MundoController.save').validator('Admin/StoreMundo')
    Route.put('mundos/:id', 'MundoController.update').validator('Admin/StoreMundo')
    Route.put('mundosimg/:id', 'MundoController.saveImage')
    Route.delete('mundos/:id', 'MundoController.delete')

    Route.get('dicionarios', 'DicionarioController.index')
    Route.get('dicionarios/:id', 'DicionarioController.show')
    Route.post('dicionarios', 'DicionarioController.save').validator('Admin/StoreDicionario')
    Route.put('dicionarios/:id', 'DicionarioController.update').validator('Admin/StoreDicionario')
    Route.delete('dicionarios/:id', 'DicionarioController.delete')

    Route.get('jogos', 'JogoController.index')
    Route.get('jogos/:id', 'JogoController.show')
    Route.post('jogos', 'JogoController.save').validator('Admin/StoreJogo')
    Route.put('jogos/:id', 'JogoController.update').validator('Admin/StoreJogo')
    Route.delete('jogos/:id', 'JogoController.delete')


    Route.get('users', 'UserController.index')
    Route.get('users/:id', 'UserController.show')
    Route.post('users', 'UserController.store')
    Route.put('users/:id', 'UserController.updateData')
    Route.put('userspass/:id', 'UserController.updatePassword')

    Route.get('dashboard', 'AdminDashboardController.index')

    Route.get('categorias', 'AdminCategoriaController.index')
    Route.get('categorias/:id', 'AdminCategoriaController.show')
    Route.post('categorias', 'AdminCategoriaController.save')
    Route.put('categorias/:id', 'AdminCategoriaController.update')
    Route.delete('categorias/:id', 'AdminCategoriaController.delete')

    Route.get('subcategorias/:categoriaid', 'AdminSubCategoriaController.index')
    Route.get('subcategorias', 'AdminSubCategoriaController.all')
    Route.get('subcategorias/:categoriaid/:id', 'AdminSubCategoriaController.show')
    Route.post('subcategorias/:categoriaid', 'AdminSubCategoriaController.save')
    Route.put('subcategorias/:categoriaid/:id', 'AdminSubCategoriaController.update')
    Route.delete('subcategorias/:categoriaid/:id', 'AdminSubCategoriaController.delete')

    Route.get('produtos', 'AdminProdutoController.index')
    Route.get('produtos/:id', 'AdminProdutoController.show')
    Route.post('produtos', 'AdminProdutoController.save')
    Route.put('produtos/:id', 'AdminProdutoController.update')
    Route.delete('produtos/:id', 'AdminProdutoController.delete')

    Route.get('unidademedidas', 'AdminUnidadeMedidaController.index')
    Route.get('unidademedidas/:id', 'AdminUnidadeMedidaController.show')
    Route.post('unidademedidas', 'AdminUnidadeMedidaController.save')
    Route.put('unidademedidas/:id', 'AdminUnidadeMedidaController.update')

    Route.get('lancamentos', 'AdminLancamentoController.index')
    Route.get('lancamentoinverter/:id', 'AdminLancamentoController.inverter')
    Route.get('lancamentos/:id', 'AdminLancamentoController.show')
    Route.post('lancamentos', 'AdminLancamentoController.save')
    Route.put('lancamentos/:id', 'AdminLancamentoController.update')
    Route.delete('lancamentos/:id', 'AdminLancamentoController.delete')
    Route.get('lancamentosforcar/:produtoid', 'AdminLancamentoController.forcarAtualizarEstoque')

    Route.get('relatorios/posicaoestoque', 'AdminRelatoriosController.posicaoEstoque')
    Route.get('relatorios/razaofisicoestoque', 'AdminRelatoriosController.razaofisicoestoque')

    /*
    Route.get('posts', 'AdminPostController.index')
    Route.get('posts/:id', 'AdminPostController.show')
    Route.post('posts', 'AdminPostController.save')
    Route.put('posts/:id', 'AdminPostController.update')
    Route.post('postsimagem/:id', 'AdminPostController.storeFotoFundo')
    Route.get('posts-ativar/:id/:active', 'AdminPostController.active')
    
    Route.post('postsitem', 'AdminPostController.storeItem')
    Route.put('postsitem/:id', 'AdminPostController.updateItem')
    Route.delete('postsitem/:id', 'AdminPostController.deleteItem')
    */


}).prefix('v1/admin').namespace('Admin')
.middleware(['auth'])
