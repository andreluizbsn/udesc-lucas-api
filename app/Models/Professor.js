'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Professor:
*      type: object
*      properties:
*        id:
*          type: integer
*        nome:
*          type: string
*        cpf:
*          type: string
*        area:
*          type: string
*        escolas:
*          type: array
*          items:
*             type: object
*             $ref: "#/definitions/Escola"
*      required:
*        - nome
*        - cpf
*        - area
*/
class Professor extends Model {

    escolas() {
        return this.belongsToMany('App/Models/ProfessorEscola').pivotTable('professor_escolas')
    }
}

module.exports = Professor
