'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Cenario:
*      type: object
*      properties:
*        id:
*          type: integer
*        nome:
*          type: string
*        url_image_base:
*          type: string
*        ativo:
*          type: boolean
*      required:
*        - nome
*        - cpf
*        - area
*        - serie
*        - escola_id
*/

/** 
*  @swagger
*  definitions:
*    CenarioSave:
*      type: object
*      properties:
*        nome:
*          type: string
*        url_image_base:
*          type: string
*        ativo:
*          type: boolean
*      required:
*        - nome
*        - url_image_base
*/

class Cenario extends Model {
}

module.exports = Cenario
