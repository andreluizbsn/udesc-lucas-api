'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** 
*  @swagger
*  definitions:
*    User:
*      type: object
*      properties:
*        id:
*          type: integer
*        nome:
*          type: string
*        email:
*          type: string
*        ativo:
*          type: boolean
*        tipo:
*          type: string
*        aluno_id:
*          type: integer
*        professor_id:
*          type: integer
*      required:
*        - nome
*        - email
*        - password
*        - tipo
*/

/** 
*  @swagger
*  definitions:
*    Register:
*      type: object
*      properties:
*        nome:
*          type: string
*        email:
*          type: string
*        password:
*          type: string
*        password_confirmation:
*          type: string
*        tipo:
*          type: string
*        aluno_id:
*          type: integer
*        professor_id:
*          type: integer
*        ativo:
*          type: boolean
*      required:
*        - nome
*        - email
*        - password
*        - password_confirmation
*        - tipo
*/

/** 
*  @swagger
*  definitions:
*    Login:
*      type: object
*      properties:
*        email:
*          type: string
*        password:
*          type: string
*      required:
*        - email
*        - password
*/

/** 
*  @swagger
*  definitions:
*    LoginAuthenticated:
*      type: object
*      properties:
*        data:
*          type: object
*          properties:
*            type:
*              type: string
*            token:
*              type: string
*            refreshToken:
*              type: string
*/

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /*
  Ocultar campos das queries
  */
 static get hidden() {
   return ['password']
 }

  static get traits () {
    return [
      '@provider:Adonis/Acl/HasRole',
      '@provider:Adonis/Acl/HasPermission'
    ]
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  image() {
    return this.belongsTo('App/Models/Image')
  }
  
  coupons() {
    return this.belongsToMany('App/Models/Coupon')
  }
}

module.exports = User
