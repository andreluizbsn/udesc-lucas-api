'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Escola:
*      type: object
*      properties:
*        id:
*          type: integer
*        user_id:
*          type: integer
*        local:
*          type: string
*        tentativas:
*          type: integer
*        finalizado:
*          type: integer
*/
class Score extends Model {
}

module.exports = Score
