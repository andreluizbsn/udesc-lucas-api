'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Jogo:
*      type: object
*      properties:
*        id:
*          type: integer
*        nome:
*          type: string
*        posicao:
*          type: integer
*        descricao:
*          type: string
*        ativo:
*          type: boolean
*        mundo_id:
*          type: integer
*        mundo:
*          type: object
*          properties:
*            id:
*              type: integer
*            nome:
*              type: string
*        user_id:
*          type: integer
*        user:
*          type: object
*          properties:
*            id:
*              type: integer
*            nome:
*              type: string
*        professor_id:
*          type: integer
*        professor:
*          type: object
*          properties:
*            id:
*              type: integer
*            nome:
*              type: string
*      required:
*        - nome
*        - posicao
*        - user_id
*        - mundo_id
*/

/** 
*  @swagger
*  definitions:
*    JogoSave:
*      type: object
*      properties:
*        nome:
*          type: string
*        posicao:
*          type: integer
*        ativo:
*          type: boolean
*        user_id:
*          type: integer
*        mundo_id:
*          type: integer
*        professor_id:
*          type: integer
*      required:
*        - nome
*        - posicao
*        - mundo_id
*        - user_id
*/

class Jogo extends Model {
}

module.exports = Jogo
