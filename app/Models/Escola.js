'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Escola:
*      type: object
*      properties:
*        id:
*          type: integer
*        nome:
*          type: string
*        cnpj:
*          type: string
*        sigla:
*          type: string
*      required:
*        - nome
*        - cnpj
*        - sigla
*/
class Escola extends Model {
}

module.exports = Escola
