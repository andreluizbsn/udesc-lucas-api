'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Release:
*      type: object
*      properties:
*        cenario:
*          type: integer
*        mundo:
*          type: integer
*        jogo:
*          type: integer
*        jogo_item:
*          type: integer
*/

class DataRelease extends Model {
}

module.exports = DataRelease
