'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Aluno:
*      type: object
*      properties:
*        id:
*          type: integer
*        nome:
*          type: string
*        email:
*          type: string
*        area:
*          type: string
*        escola_id:
*          type: integer
*        escola:
*          type: object
*          properties:
*            id:
*              type: integer
*            nome:
*              type: string
*            email:
*              type: string
*            sigla:
*              type: string
*      required:
*        - nome
*        - email
*        - area
*        - serie
*        - escola_id
*/

/** 
*  @swagger
*  definitions:
*    AlunoSave:
*      type: object
*      properties:
*        nome:
*          type: string
*        email:
*          type: string
*        area:
*          type: string
*        escola_id:
*          type: integer
*      required:
*        - nome
*        - email
*        - area
*        - serie
*        - escola_id
*/

class Aluno extends Model {
}

module.exports = Aluno
