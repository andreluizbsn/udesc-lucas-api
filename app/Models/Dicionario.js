'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Dicionario:
*      type: object
*      properties:
*        id:
*          type: integer
*        chave:
*          type: string
*        valor:
*          type: string
*      required:
*        - chave
*        - valor
*/

/** 
*  @swagger
*  definitions:
*    CenarioSave:
*      type: object
*      properties:
*        chave:
*          type: string
*        valor:
*          type: string
*      required:
*        - chave
*        - valor
*/

class Dicionario extends Model {
}

module.exports = Dicionario
