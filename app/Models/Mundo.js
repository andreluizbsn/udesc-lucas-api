'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** 
*  @swagger
*  definitions:
*    Mundo:
*      type: object
*      properties:
*        id:
*          type: integer
*        nome:
*          type: string
*        url_image_base:
*          type: string
*        posicao:
*          type: integer
*        ativo:
*          type: boolean
*        cenario_id:
*          type: integer
*        cenario:
*          type: object
*          properties:
*            id:
*              type: integer
*            nome:
*              type: string
*      required:
*        - nome
*        - url_image_base
*        - posicao
*        - cenario_id
*/

/** 
*  @swagger
*  definitions:
*    JogoSave:
*      type: object
*      properties:
*        nome:
*          type: string
*        url_image_base:
*          type: string
*        posicao:
*          type: integer
*        ativo:
*          type: boolean
*        cenario_id:
*          type: integer
*      required:
*        - nome
*        - url_image_base
*        - posicao
*        - cenario_id
*/

class Mundo extends Model {
}

module.exports = Mundo
