'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Escola = use('App/Models/Escola')
const Transformer = use('App/Transformers/Admin/EscolaTransformer')

class EscolaController {
  
/**
* @swagger
* /admin/escolas:
*   get:
*     tags:
*       - Escola
*     summary: Lista de escolas
*     description: Lista de escolas paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes e siglas que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Escola"
*/
async index ({ request, response }) {

  var queries = request.all();

  var dados;

  var query = Escola.query().select(["nome", "id", "sigla"])

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    query.orWhere('sigla', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('nome', 'asc')
  
  var dados = await query.fetch()

  return response.send(dados)

}

}

module.exports = EscolaController
