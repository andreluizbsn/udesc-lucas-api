'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Score = use('App/Models/Score')
const Service = use('App/Services/ScoreService/ScoreService')

class ScoreController {
  
/**
* @swagger
* /admin/alunos:
*   get:
*     tags:
*       - Aluno
*     summary: Lista de aluno
*     description: Lista de alunos paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Aluno"
*/
async index ({ request, response, auth }) {

  var user = await auth.getUser();

  const service = new Service();
  var retorno = await service.getAppData(user.id);

  return response.send(retorno)

}

/**
  * @swagger
  * /admin/alunos:
  *   post:
  *     tags:
  *       - Aluno
  *     summary: Salvar de alunos
  *     description: Salvar de alunos;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/AlunoSave"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Aluno"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, auth }) {

    const { dados } = request.all();
    var user = await auth.getUser();

    const service = new Service();
    var retorno = await service.syncData(user.id, dados);

    return response.status(201).send(retorno)
    
  }

}

module.exports = ScoreController
