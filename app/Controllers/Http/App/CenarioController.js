'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Cenario = use('App/Models/Cenario')
const Transformer = use('App/Transformers/Admin/CenarioTransformer')
const Env = use('Env')

class CenarioController {
  
/**
* @swagger
* /admin/cenarios:
*   get:
*     tags:
*       - Cenário
*     summary: Lista de cenários
*     description: Lista de cenarios paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Cenario"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados;

  var query = Cenario.query()

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('nome', 'asc')

  var dados = await query.fetch();

  dados = await transform.collection(dados, Transformer)

  var urlImageBase = 'http://' + Env.get('HOST') + ':' + Env.get('PORT') + '/uploads/cenarios/';
  dados.forEach(dado => {
    dado.image_logo = dado.image_logo ? urlImageBase + dado.id + '/' + dado.image_logo : null;
    dado.image_backgound = dado.image_backgound ?  urlImageBase + dado.id + '/' + dado.image_backgound : null;    
  });
  
  return response.send(dados)

}


}

module.exports = CenarioController
