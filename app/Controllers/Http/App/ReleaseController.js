'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const DataRelease = use('App/Models/DataRelease')

class ReleaseController {
  
/**
* @swagger
* /admin/release:
*   get:
*     tags:
*       - Release
*     summary: Lista de ultimas aplicações
*     description: Lista de ultimas aplicações;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           $ref: "#/definitions/Release"
*/
async index ({ request, response, transform, pagination }) {

  var dados;

  var query = await DataRelease.all()

  if ( query.rows.length != 0 ) {
    dados = query.rows[0];
  } else {
    dados = new DataRelease();
    dados.cenario = 0;
    dados.mundo = 0;
    dados.jogo = 0;
    dados.jogo_item = 0;
    await dados.save();
    
  }

  return response.send(dados)

}


}

module.exports = ReleaseController
