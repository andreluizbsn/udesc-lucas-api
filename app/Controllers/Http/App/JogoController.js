'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Jogo = use('App/Models/Jogo')
const JogoItem = use('App/Models/JogoItem')
const Service = use('App/Services/Jogos/JogoService')
const Mundo = use('App/Models/Mundo')
const User = use('App/Models/User')
const Professor = use('App/Models/Professor')
const Transformer = use('App/Transformers/Admin/JogoTransformer')
const TransformerMundo = use('App/Transformers/Admin/MundoSmallTransformer')
const TransformerUser = use('App/Transformers/Admin/UserTransformer')
const TransformerProfessor = use('App/Transformers/Admin/ProfessorTransformer')

class JogoController {
  
/**
* @swagger
* /admin/jogos:
*   get:
*     tags:
*       - Jogo
*     summary: Lista de jogo
*     description: Lista de jogos paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Jogo"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var query = Jogo.query()

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('posicao', 'asc')

  var dados = await query.fetch();

  dados.rows.forEach(d => {
    d.ativo = d.ativo ? 1 : 0;
  });

  return response.send(dados)

}

async items ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados = await JogoItem.all()
  dados.rows.forEach(d => {
    d.verdadeiro = d.verdadeiro ? 1 : 0;
  });
  return response.send(dados)

}
}

module.exports = JogoController
