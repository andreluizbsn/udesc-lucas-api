'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Mundo = use('App/Models/Mundo')
const Cenario = use('App/Models/Cenario')
const Transformer = use('App/Transformers/Admin/MundoTransformer')
const TransformerCenario = use('App/Transformers/Admin/CenarioTransformer')

class MundoController {
  
/**
* @swagger
* /admin/mundos:
*   get:
*     tags:
*       - Mundo
*     summary: Lista de mundo
*     description: Lista de mundos paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Mundo"
*/
  async index ({ request, response, transform, pagination }) {

    var queries = request.all();

    var dados;

    var query = Mundo.query()

    if ( queries.filtro ) {

      query.where('nome', 'ILIKE', `%${queries.filtro}%`)
      
    }

    query.orderBy('posicao', 'asc')

    var dados = await query.fetch();

    dados = await transform.collection(dados, Transformer)

    dados.forEach(d => {
      d.ativo = d.ativo ? 1 : 0;
    });

    return response.send(dados) 

  }
}

module.exports = MundoController
