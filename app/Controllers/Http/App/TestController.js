'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


class TestController {

  async auth ({ request, response, auth }) {

    var dados = {
      'ok': true,
      'msg': 'Test with Auth'
    }

    return response.send(dados)

  }

  async noauth ({ request, response, auth }) {

    var dados = {
      'ok': true,
      'msg': 'Test without Auth'
    }

    return response.send(dados)

  }
  
}

module.exports = TestController
