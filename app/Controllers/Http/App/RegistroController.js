'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const User = use('App/Models/User')
const Aluno = use('App/Models/Aluno')

class RegistroController {

  async index ({ request, response }) {

    var { nome, email, password, password_confirmation, serie, escola_id } = request.all();

    var strMessage = "";
    var isOk = true;

    if ( !nome ) strMessage = "O nome precisa ser preenchido! ";
    if ( !email ) strMessage += "O E-Mail precisa ser preenchido! ";
    if ( !password ) strMessage += "A senha precisa ser preenchida! ";
    if ( !password_confirmation ) strMessage += "A confirmação de senha precisa ser preenchida! ";
    if ( !serie ) strMessage += "A Série precisa ser preenchida! ";
    if ( !escola_id ) strMessage += "A escola precisa ser preenchida! ";

    if ( strMessage != "" ) {
      isOk = false;
    } else {

      if ( password != password_confirmation ) {
        strMessage += "A senha e a confirmação de senha precisam ser iguais! ";
        isOk = false;
      } else {
        //verifica se já existe algum usuário com o mesmo email
        var userRep = await User.query().where('email', email).first();
        if ( userRep ) {
          strMessage += "O E-Mail " + email + " já está cadastrado para outro usuário! " + userRep.nome;
          isOk = false;
        } else {

          //cadastrar aluno e usuario
          try {
            var aluno = Aluno.create({nome, email, serie, escola_id});
            var tipo = "ALUNO";
            var ativo = true;
            var aluno_id = aluno.id;
            await User.create( { nome, email, tipo, ativo, aluno_id, password })

            strMessage = "Registro efetuado com sucesso!"

          } catch (error) {
            
            strMessage = "Erro ao efetuar o registro! " + error.message;
            isOk = false;

          }

        }
      }

    }

    return response.send({
      "ok": isOk,
      "message": strMessage
    }) 

  }
}

module.exports = RegistroController
