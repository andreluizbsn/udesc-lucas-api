'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Post = use('App/Models/Post')
const PostItem = use('App/Models/PostItem')
const Categoria = use('App/Models/Categoria')
const Subcategoria = use('App/Models/Subcategoria')
const Database = use('Database')
const StringService = use('App/Services/String/StringService')
const FilesService = use('App/Services/Sincronizacao/FilesService')
const Env = use('Env')

class PostController {
  
  async index ({ request, response }) {

    const queries = request.all()

    var dadosQr = Post.query()
    .where('active', true)
    .orderBy('id', 'desc')
    
    if ( queries.filtro ) {
      dadosQr.where('titulo', 'like', '%'+(queries.filtro.split(' ').join('%'))+'%');
      dadosQr.orWhere('resumo', 'like', '%'+(queries.filtro.split(' ').join('%'))+'%');
    }

    if ( queries.tag ) {
      dadosQr.where('tags', 'like', '%#'+(queries.tag)+'%');
    }

    var dados = await dadosQr.paginate(queries.page || 1, 10)

    //capturar as Categorias e Subcategorias
    var categoriasIds = [];
    var subCategoriasIds = [];
    dados.rows.forEach(p => {
      if ( p.categoria_id )
        categoriasIds.push( p.categoria_id )
      if ( p.subcategoria_id )
        subCategoriasIds.push( p.subcategoria_id )
    });

    //buscar categorias
    var categorias = await Categoria.query().whereIn('id', categoriasIds).fetch();

    //buscar subcategorias
    var subcategorias = await Subcategoria.query().whereIn('id', subCategoriasIds).fetch();

    dados.rows.forEach(p => {
      categorias.rows.forEach(cat => {
        if ( cat.id == p.categoria_id ) {
          p.categoria = cat.nome;
        }
      });
      subcategorias.rows.forEach(cat => {
        if ( cat.id == p.subcategoria_id ) {
          p.subcategoria = cat.nome;
        }
      });
    });

    return response.send(dados)

  }

  async show ({ params: { titulo }, request, response, transform }) {

    var dados = await Post.query().where({'url': titulo, 'active': true}).first();

    if ( !dados ) {
      return response.status(404).send({'message': 'Não encontrado o conteúdo!'})  
    }

    var itens = await PostItem.query().where('post_id', dados.id).orderBy('sequencia', 'asc').fetch()

    dados.itens = itens.rows;

    return response.send(dados)
    
  }

  async suggestions ({  params: { id }, request, response }) {

    const queries = request.all()

    var dadosBase = await Post.find( id );

    var dados = await Post.query()
    .where({'categoria_id': dadosBase.categoria_id, 'active': true})
    .andWhere('id', '<>', id)
    .paginate(queries.page || 1, 10)

    //capturar as Categorias e Subcategorias
    var categoriasIds = [];
    var subCategoriasIds = [];
    dados.rows.forEach(p => {
      if ( p.categoria_id )
        categoriasIds.push( p.categoria_id )
      if ( p.subcategoria_id )
        subCategoriasIds.push( p.subcategoria_id )
    });

    //buscar categorias
    var categorias = await Categoria.query().whereIn('id', categoriasIds).fetch();

    //buscar subcategorias
    var subcategorias = await Subcategoria.query().whereIn('id', subCategoriasIds).fetch();

    dados.rows.forEach(p => {
      categorias.rows.forEach(cat => {
        if ( cat.id == p.categoria_id ) {
          p.categoria = cat.nome;
        }
      });
      subcategorias.rows.forEach(cat => {
        if ( cat.id == p.subcategoria_id ) {
          p.subcategoria = cat.nome;
        }
      });
    });

    return response.send(dados)

  }
}

module.exports = PostController
