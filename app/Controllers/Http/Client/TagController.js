'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Tag = use('App/Models/Tag')

class TagController {
  
  async index ({ request, response }) {

    var dados = await Tag.all()

    return response.send(dados)

  }

}

module.exports = TagController
