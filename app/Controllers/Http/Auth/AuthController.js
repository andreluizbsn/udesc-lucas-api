'use strict'

const Database = use('Database')
const User = use('App/Models/User')
const Role = use('Role')
const Ws = use('Ws')
const Service = use('App/Services/ScoreService/ScoreService')

class AuthController {

  /**
  * @swagger
  * /auth/register:
  *   post:
  *     tags:
  *       - Autenticação
  *     summary: Registro no sistema
  *     description: Registro no sistema
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/Register"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/User"
  *       404:
  *         description: Registro não encontrado
  * 
*/

    async register({ request, response }) {
        const trx = await Database.beginTransaction()

        try {
            const { nome, email, password, tipo, aluno_id, professor_id, ativo } = request.all()

            const user = await User.create({ nome, email, password, tipo, aluno_id, professor_id, ativo }, trx)

            await trx.commit()

            return response.status(201).send({ data: user })

        } catch (error) {

            await trx.rollback()

            return response.status(400).send({
                message: 'Erro ao realizar o cadastro!'
            })
        }
    }

     
  /**
  * @swagger
  * /auth/login:
  *   post:
  *     tags:
  *       - Autenticação
  *     summary: Login no sistema
  *     description: Autenticação no sistema
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/Login"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/LoginAuthenticated"
  *       404:
  *         description: Registro não encontrado
  * 
*/

    async login({ request, response, auth }) {

        const { email, password } = request.all()

        let data = await auth.withRefreshToken().attempt(email, password);

        if ( data ) {
            var user = await User.query()
            .select(['id', 'nome', 'email', 'ativo'])
            .where('email', email)
            .first();
        }

        const service = new Service();
        const score = await service.getAppData(user.id)

        return response.send({'data': data, 'user': user, 'score': score})

    }

    async refresh({ request, response, auth }) {
        
        var refresh_token = request.input('refresh_token')

        if ( !refresh_token ) {
            refresh_token = request.header('refresh_token')
        }

        const user = await auth.newRefreshToken().generateForRefreshToken(refresh_token)

        return response.send({data: user})

    }

    async logout({ request, response, auth }) {

        console.log(request.headers);
        
        var refresh_token = request.input('refresh_token')

        if ( !refresh_token ) {
            refresh_token = request.header('refresh_token')
        }

        await auth
        .authenticator('jwt')
        .revokeTokens([refreshToken], true)

        return response.status(204).send({})

    }

    async forgot({ request, response }) {

    }

    async remember({ request, response }) {

    }

    async reset({ request, response }) {

    }
}

module.exports = AuthController
