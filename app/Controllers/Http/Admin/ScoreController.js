'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Aluno = use('App/Models/Aluno')
const Escola = use('App/Models/Escola')
const Transformer = use('App/Transformers/Admin/AlunoTransformer')
const TransformerEscola = use('App/Transformers/Admin/EscolaTransformer')

class AlunoController {
  
/**
* @swagger
* /admin/alunos:
*   get:
*     tags:
*       - Aluno
*     summary: Lista de aluno
*     description: Lista de alunos paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Aluno"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados;

  var query = Aluno.query()

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('nome', 'asc')

  var dados;
  var idsEsc = [];

  if ( !queries.no_paginate ) {
    dados = await query.paginate( pagination.page, pagination.limit )
  
    dados.rows.forEach(al => {
      idsEsc.push( al.escola_id );
    });

    dados = await transform.include('escola').paginate(dados, Transformer)
  
  } else {
    dados = await query.fetch();

    dados.rows.forEach(al => {
      idsEsc.push( al.escola_id );
    });
  }

  

  var escolas = await Escola.query().whereIn('id', idsEsc).fetch();
  escolas = await transform.collection( escolas, TransformerEscola )

  if ( !queries.no_paginate ) {
    dados.data.forEach(al => {
      escolas.forEach(e => {
        if ( al.escola_id == e.id ) {
          al.escola = e;
        }
      });
    });
  } else {
    dados.rows.forEach(al => {
      escolas.forEach(e => {
        if ( al.escola_id == e.id ) {
          al.escola = e;
        }
      });
    });
  }

  return response.send(dados)

}
  
/**
  * @swagger
  * /admin/alunos/{id}:
  *   get:
  *     tags:
  *       - Aluno
  *     summary: Detalhe de alunos
  *     description: Detalhe de alunos;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Aluno"
  *       404:
  *         description: Registro não encontrado
  * components:
  *   schemas:
  *     Aluno:
  *       type: Aluno
*/
async show ({ params: { id }, request, response, transform }) {

    var dados = await Aluno.findOrFail(id)
    //busca a ligação entre aluno e escolas
    var escola = await Escola.findOrFail( dados.escola_id );

    dados = await transform.item(dados, Transformer)

    dados.escola = await transform.item(escola, TransformerEscola);

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/alunos:
  *   post:
  *     tags:
  *       - Aluno
  *     summary: Salvar de alunos
  *     description: Salvar de alunos;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/AlunoSave"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Aluno"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, transform }) {

    try {
      
      const { nome, email, serie, escola_id } = request.all()

      /*if ( cpf ) {
        if ( cpf.length != 11 ) {
          response.status(400).send(
            {
              "errors": [
                {
                  "message": "O CPF deve ter 11 caracteres!",
                  "field": "cpf",
                  "validation": "cpf"
                }
              ]
            }
          )  
          return;
        } else if ( isNaN( cpf ) ) { 
          response.status(400).send(
            {
              "errors": [
                {
                  "message": "O CPF deve conter apenas números!",
                  "field": "cpf",
                  "validation": "cpf"
                }
              ]
            })  
          return;
        }
      }*/

      var dado = await Aluno.create( { nome, email, serie, escola_id })

      dado = await transform.item(dado, Transformer)

      var escola = await Escola.findOrFail( dado.escola_id );

      dado.escola = await transform.item(escola, TransformerEscola);

      return response.status(201).send(dado)



    } catch (error) {
      
      response.status(400).send({
        "errors": [
          {
            "message": "Erro ao processar a sua solicitação! " + error,
            "field": "email",
            "validation": "email"
          }
        ]
      })

    }
    
  }

/**
  * @swagger
  * /admin/alunos/{id}:
  *   put:
  *     tags:
  *       - Aluno
  *     summary: Alterar de alunos
  *     description: Alterar dados cadastrais;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         description: Pode ser alterado o nome e/ou ativo
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/AlunoSave"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Aluno"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async update ({ params: { id }, request, response, transform }) {

    var { nome, email, serie, escola_id } = request.all();

    /*if ( cpf ) {
      if ( cpf.length != 11 ) {
        response.status(400).send(
          {
            "errors": [
              {
                "message": "O CPF deve ter 11 caracteres!",
                "field": "cpf",
                "validation": "cpf"
              }
            ]
          }
        )  
        return;
      } else if ( isNaN( cpf ) ) { 
        response.status(400).send(
          {
            "errors": [
              {
                "message": "O CPF deve conter apenas números!",
                "field": "cpf",
                "validation": "cpf"
              }
            ]
          })  
        return;
      } else {*/
        var exdado = await Aluno.query()
        .where('email', email)
        .fetch();

        if ( exdado.rows.length != 0 ) {
          var otherdado = false;
          exdado.rows.forEach(d => {
            if ( d.id != id ) {
              otherdado = true;
            }
          });
          if ( otherdado ) {
            response.status(400).send(
              {
                "errors": [
                  {
                    "message": "O E-mail já está sendo usado em outro cadastro!",
                    "field": "email",
                    "validation": "email"
                  }
                ]
              })  
            return;
          }
        }
      //} 
    //}

    var dados = await Aluno.findOrFail( id );
    dados.merge({ nome, email, serie, escola_id });

    await dados.save();

    var escola = await Escola.findOrFail( dados.escola_id );

    dados = await transform.item(dados, Transformer)

    dados.escola = await transform.item(escola, TransformerEscola);

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/alunos/{id}:
  *   delete:
  *     tags:
  *       - Aluno
  *     summary: Excluir aluno
  *     description: Excluir registro de aluno;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       204:
  *         description: Requisição finalizada
  *       404:
  *         description: Registro não encontrado
*/
  async delete ({ params: { id }, request, response, transform }) {

    var dado = await Aluno.findOrFail( id );
    
    await dado.delete();

    return response.status(204).send()
    
  }

}

module.exports = AlunoController
