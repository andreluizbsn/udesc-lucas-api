'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Escola = use('App/Models/Escola')
const Transformer = use('App/Transformers/Admin/EscolaTransformer')

class EscolaController {
  
/**
* @swagger
* /admin/escolas:
*   get:
*     tags:
*       - Escola
*     summary: Lista de escolas
*     description: Lista de escolas paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes e siglas que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Escola"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados;

  var query = Escola.query()

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    query.orWhere('sigla', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('nome', 'asc')
  
  var dados;

  if ( !queries.no_paginate ) {
    dados = await query.paginate( pagination.page, pagination.limit )

    dados = await transform.paginate(dados, Transformer)
  } else {
    dados = await query.fetch()

    dados = await transform.collection(dados, Transformer)
  }

  return response.send(dados)

}


  
/**
  * @swagger
  * /admin/escolas/{id}:
  *   get:
  *     tags:
  *       - Escola
  *     summary: Detalhe de escolas
  *     description: Detalhe de escolas;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Escola"
  *       404:
  *         description: Registro não encontrado
  * components:
  *   schemas:
  *     Escola:
  *       type: Escola
*/
async show ({ params: { id }, request, response, transform }) {

    var dados = await Escola.findOrFail(id)

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/escolas:
  *   post:
  *     tags:
  *       - Escola
  *     summary: Salvar de escolas
  *     description: Salvar de escolas;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/Escola"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Escola"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cnpj"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, transform }) {

    try {
      
      const { nome, cnpj, sigla } = request.all()

      if ( cnpj ) {
        if ( cnpj.length != 14 ) {
          response.status(400).send(
            {
              "errors": [
                {
                  "message": "O CNPJ deve ter 14 caracteres!",
                  "field": "cnpj",
                  "validation": "cnpj"
                }
              ]
            }
          )  
          return;
        } else if ( isNaN( cnpj ) ) { 
          response.status(400).send(
            {
              "errors": [
                {
                  "message": "O CNPJ deve conter apenas números!",
                  "field": "cnpj",
                  "validation": "cnpj"
                }
              ]
            })  
          return;
        }
      }

      var dado = await Escola.create( { nome, cnpj, sigla })

      dado = await transform.item(dado, Transformer)

      return response.status(201).send(dado)

    } catch (error) {
      
      response.status(400).send({
        message: "Erro ao processar a sua solicitação!"
      })

    }
    
  }

/**
  * @swagger
  * /admin/escolas/{id}:
  *   put:
  *     tags:
  *       - Escola
  *     summary: Alterar de escolas
  *     description: Alterar dados cadastrais;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         description: Pode ser alterado o nome e/ou ativo
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/Escola"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Escola"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cnpj"
  *              }
  *            ]
  *          }
*/
async update ({ params: { id }, request, response, transform }) {

    var { nome, cnpj, sigla } = request.all();

    if ( cnpj ) {
      if ( cnpj.length != 14 ) {
        response.status(400).send(
          {
            "errors": [
              {
                "message": "O CNPJ deve ter 14 caracteres!",
                "field": "cnpj",
                "validation": "cnpj"
              }
            ]
          }
        )  
        return;
      } else if ( isNaN( cnpj ) ) { 
        response.status(400).send(
          {
            "errors": [
              {
                "message": "O CNPJ deve conter apenas números!",
                "field": "cnpj",
                "validation": "cnpj"
              }
            ]
          })  
        return;
      } else {
        var exdado = await Escola.query()
        .where('cnpj', cnpj)
        .fetch();

        if ( exdado.rows.length != 0 ) {
          var otherdado = false;
          exdado.rows.forEach(d => {
            if ( d.id != id ) {
              otherdado = true;
            }
          });
          if ( otherdado ) {
            response.status(400).send(
              {
                "errors": [
                  {
                    "message": "O CNPJ já está sendo usado em outro cadastro!",
                    "field": "cnpj",
                    "validation": "cnpj"
                  }
                ]
              })  
            return;
          }
        }
      } 
    }

    var dados = await Escola.findOrFail( id );
    dados.merge({ nome, cnpj, sigla });
    await dados.save();

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/escolas/{id}:
  *   delete:
  *     tags:
  *       - Escola
  *     summary: Excluir escola
  *     description: Excluir registro de escola;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       204:
  *         description: Requisição finalizada
  *       404:
  *         description: Registro não encontrado
*/
  async delete ({ params: { id }, request, response, transform }) {

    var dado = await Escola.findOrFail( id );
    
    await dado.delete();

    return response.status(204).send()
    
  }

}

module.exports = EscolaController
