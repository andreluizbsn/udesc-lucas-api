'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Dicionario = use('App/Models/Dicionario')
const Transformer = use('App/Transformers/Admin/DicionarioTransformer')

class DicionarioController {
  
/**
* @swagger
* /admin/dicionarios:
*   get:
*     tags:
*       - Cenário
*     summary: Lista de cenários
*     description: Lista de dicionarios paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Dicionario"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados;

  var query = Dicionario.query()

  if ( queries.filtro ) {

    query.where('chave', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('chave', 'asc')

  var dados;

  if ( !queries.no_paginate ) {

    dados = await query.paginate( pagination.page, pagination.limit )
    dados = await transform.paginate(dados, Transformer)

  } else {
    dados = await query.fetch()

    dados = await transform.collection(dados, Transformer)
  }

  return response.send(dados)

}
  
/**
  * @swagger
  * /admin/dicionarios/{id}:
  *   get:
  *     tags:
  *       - Dicionario
  *     summary: Detalhe de dicionarios
  *     description: Detalhe de dicionarios;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Dicionario"
  *       404:
  *         description: Registro não encontrado
  * components:
  *   schemas:
  *     Dicionario:
  *       type: Dicionario
*/
async show ({ params: { id }, request, response, transform }) {

    var dados = await Dicionario.findOrFail(id)

    dados = await transform.item(dados, Transformer)

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/dicionarios:
  *   post:
  *     tags:
  *       - Dicionario
  *     summary: Salvar de dicionarios
  *     description: Salvar de dicionarios;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/DicionarioSave"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Dicionario"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, transform }) {

    try {
      
      const { chave, valor } = request.all()

      var dadoExistente = await Dicionario.query().where('chave', chave).first();

      if ( dadoExistente ) {
        dadoExistente = await transform.item(dadoExistente, Transformer);
        return response.status(201).send(dadoExistente)
      }

      var dado = await Dicionario.create( { chave, valor })

      dado = await transform.item(dado, Transformer)

      return response.status(201).send(dado)



    } catch (error) {
      
      response.status(400).send({
        "errors": [
          {
            "message": "Erro ao processar a sua solicitação! " + error,
            "field": "cpf",
            "validation": "cpf"
          }
        ]
      })

    }
    
  }

/**
  * @swagger
  * /admin/dicionarios/{id}:
  *   put:
  *     tags:
  *       - Dicionario
  *     summary: Alterar de dicionarios
  *     description: Alterar dados cadastrais;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         description: Pode ser alterado o nome e/ou ativo
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/DicionarioSave"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Dicionario"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async update ({ params: { id }, request, response, transform }) {

    var { chave, valor } = request.all();

    var dados = await Dicionario.findOrFail( id );
    dados.merge({ chave, valor });

    await dados.save();

    dados = await transform.item(dados, Transformer)

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/dicionarios/{id}:
  *   delete:
  *     tags:
  *       - Dicionario
  *     summary: Excluir dicionario
  *     description: Excluir registro de dicionario;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       204:
  *         description: Requisição finalizada
  *       404:
  *         description: Registro não encontrado
*/
  async delete ({ params: { id }, request, response, transform }) {

    var dado = await Dicionario.findOrFail( id );
    
    await dado.delete();

    return response.status(204).send()
    
  }

}

module.exports = DicionarioController
