'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
 const Env = use('Env')
 const FilesService = use('App/Services/Sincronizacao/FilesService')
 const Database = use('Database')

class ImageController {
  
  async store({ params: {local, id, image_field}, request, response, auth }) {
    
    try {
      const imageFile = request.file('image', {
          types: ['image'],
          size: '1mb'
      })

      if ( !imageFile ) {
        return response.send({'msg': 'Nothing to save'})  
      } 

      //criar nomes para os arquivos
      var filesService = new FilesService();

      //var nomeArquivoThumb = filesService.fileNameGen( 50 )  + '.' + imageThumbnail.extname;
      var fileNameNew = filesService.fileNameGen( 50 )  + '.' + imageFile.extname;
      //montar local para upload
      var urlImagem = './public/uploads/'+local+'/'+id;

      //upload pdf
      await imageFile.move(urlImagem, {
          name: fileNameNew
      })

      if (!imageFile.moved()) {
          console.log(imageFile._error);
          return response.status(400).send( imageFile._error )
      }

      var sqlImage = "SELECT " + image_field + " FROM " + local + " where id = " + id;
      var { rows } = await Database.raw( sqlImage );
      var oldImageName = rows[0][image_field];

      if ( oldImageName ) {
        await filesService.delete ( local, id, oldImageName ) 
      }

      var sqlUpdate = "UPDATE " + local + " SET " + image_field + " = '" + fileNameNew + "' where id = " + id;

      await Database.raw( sqlUpdate );

      return response.send({'msg': 'updated ok'})  
    } catch (error) {
      return response.status(400).send( error )
    }    
  }
}

module.exports = ImageController
