'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Cenario = use('App/Models/Cenario')
const Transformer = use('App/Transformers/Admin/CenarioTransformer')
const Env = use('Env')

class CenarioController {
  
/**
* @swagger
* /admin/cenarios:
*   get:
*     tags:
*       - Cenário
*     summary: Lista de cenários
*     description: Lista de cenarios paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Cenario"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados;

  var query = Cenario.query().select(['id', 'nome', 'ativo', 'image_logo', 'image_backgound'])

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('nome', 'asc')

  var dados;

  if ( !queries.no_paginate ) {

    dados = await query.paginate( pagination.page, pagination.limit )

    dados = await transform.paginate(dados, Transformer)

  } else {
    dados = await query.fetch()

    dados = await transform.collection(dados, Transformer)
  }

  return response.send(dados)

}
  
/**
  * @swagger
  * /admin/cenarios/{id}:
  *   get:
  *     tags:
  *       - Cenario
  *     summary: Detalhe de cenarios
  *     description: Detalhe de cenarios;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Cenario"
  *       404:
  *         description: Registro não encontrado
  * components:
  *   schemas:
  *     Cenario:
  *       type: Cenario
*/
async show ({ params: { id }, request, response, transform }) {

    var dados = await Cenario.findOrFail(id)

    var urlImageBase = 'http://' + Env.get('HOST') + ':' + Env.get('PORT') + '/uploads/cenarios/' + id + '/';
    dados.image_logo = dados.image_logo ? urlImageBase + dados.image_logo : null;
    dados.image_backgound = dados.image_backgound ? urlImageBase + dados.image_backgound : null;

    dados = await transform.item(dados, Transformer)

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/cenarios:
  *   post:
  *     tags:
  *       - Cenario
  *     summary: Salvar de cenarios
  *     description: Salvar de cenarios;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/AlunoSave"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Cenario"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, transform }) {

    try {
      
      const { nome, ativo } = request.all()

      var dado = await Cenario.create( { nome, ativo })

      dado = await transform.item(dado, Transformer)

      return response.status(201).send(dado)



    } catch (error) {
      
      response.status(400).send({
        "errors": [
          {
            "message": "Erro ao processar a sua solicitação! " + error,
            "field": "cpf",
            "validation": "cpf"
          }
        ]
      })

    }
    
  }

/**
  * @swagger
  * /admin/cenarios/{id}:
  *   put:
  *     tags:
  *       - Cenario
  *     summary: Alterar de cenarios
  *     description: Alterar dados cadastrais;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         description: Pode ser alterado o nome e/ou ativo
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/AlunoSave"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Cenario"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async update ({ params: { id }, request, response, transform }) {

    var { nome, ativo } = request.all();

    var dados = await Cenario.findOrFail( id );
    dados.merge({ nome, ativo });

    await dados.save();

    dados = await transform.item(dados, Transformer)

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/cenarios/{id}:
  *   delete:
  *     tags:
  *       - Cenario
  *     summary: Excluir cenario
  *     description: Excluir registro de cenario;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       204:
  *         description: Requisição finalizada
  *       404:
  *         description: Registro não encontrado
*/
  async delete ({ params: { id }, request, response, transform }) {

    var dado = await Cenario.findOrFail( id );
    
    await dado.delete();

    return response.status(204).send()
    
  }

}

module.exports = CenarioController
