'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Jogo = use('App/Models/Jogo')
const JogoItem = use('App/Models/JogoItem')
const Service = use('App/Services/Jogos/JogoService')
const Mundo = use('App/Models/Mundo')
const User = use('App/Models/User')
const Professor = use('App/Models/Professor')
const Transformer = use('App/Transformers/Admin/JogoTransformer')
const TransformerMundo = use('App/Transformers/Admin/MundoSmallTransformer')
const TransformerUser = use('App/Transformers/Admin/UserTransformer')
const TransformerProfessor = use('App/Transformers/Admin/ProfessorTransformer')

class JogoController {
  
/**
* @swagger
* /admin/jogos:
*   get:
*     tags:
*       - Jogo
*     summary: Lista de jogo
*     description: Lista de jogos paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Jogo"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados;

  var query = Jogo.query()

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('posicao', 'asc')

  var dados = await query.paginate( pagination.page, pagination.limit )
  dados = await transform.paginate(dados, Transformer)

  if ( dados.data.length != 0 ) {
    var idsUsers = [];
    var idsMundos = [];
    var idsProfs = [];

    dados.data.forEach(al => {
      idsUsers.push( al.user_id );
      idsMundos.push( al.mundo_id );
      if ( al.professor_id ) {
        idsProfs.push( al.professor_id );
      }
    });

    var mundos = await Mundo.query().whereIn('id', idsMundos).fetch();
    mundos = await transform.collection( mundos, TransformerMundo )

    var users = await User.query().whereIn('id', idsUsers).fetch();
    users = await transform.collection( users, TransformerUser )

    var professores = [];
    if ( idsProfs.length != 0 ) {
      professores = await Professor.query().whereIn('id', idsProfs).fetch();
      professores = await transform.collection( professores, TransformerProfessor )
    }

    dados.data.forEach(al => {
      mundos.forEach(e => {
        if ( al.mundo_id == e.id ) {
          al.mundo = e;
        }
      });
      users.forEach(e => {
        if ( al.user_id == e.id ) {
          al.user = e;
        }
      });
      if ( al.professor_id ) {
        professores.forEach(e => {
          if ( al.professor_id == e.id ) {
            al.professor = e;
          }
        });
      }
    });
  }

  return response.send(dados)

}
  
/**
  * @swagger
  * /admin/jogos/{id}:
  *   get:
  *     tags:
  *       - Jogo
  *     summary: Detalhe de jogos
  *     description: Detalhe de jogos;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Jogo"
  *       404:
  *         description: Registro não encontrado
  * components:
  *   schemas:
  *     Jogo:
  *       type: Jogo
*/
async show ({ params: { id }, request, response, transform }) {

    var dado = await Jogo.findOrFail(id)
    dado.items = [];
    //busca a ligação entre jogo e cenarios
    var mundo = await Mundo.findOrFail( dado.mundo_id );
    dado.mundo = await transform.item(mundo, TransformerMundo);

    if ( dado.user_id && dado.user_id != 0 ) {
      var user = await User.findOrFail( dado.user_id );
      dado.user = await transform.item(user, TransformerUser);
    }

    if ( dado.professor_id ) {
      var professor = await Professor.findOrFail( dado.professor_id );
      dado.professor = await transform.item(professor, TransformerProfessor);
    }

    var items = await JogoItem.query().where('jogo_id', dado.id).fetch();

    if ( items.rows.length != 0 ) {
      dado.items = items.rows
    }

    return response.send(dado)
    
  }

/**
  * @swagger
  * /admin/jogos:
  *   post:
  *     tags:
  *       - Jogo
  *     summary: Salvar de jogos
  *     description: Salvar de jogos;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/JogoSave"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Jogo"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, transform }) {

    try {
      
      const { mundo_id, nome, posicao, descricao, json, ativo, user_id, professor_id, grupo, tipo, items } = request.all()

      var dado = await Jogo.create( { mundo_id, nome, posicao, descricao, json, ativo, user_id, professor_id, grupo, tipo })

      dado = await transform.item(dado, Transformer)

      var service = new Service();
      var itemsRet = await service.salvarItems( dado.id, items )

      dado.items = itemsRet;

      var mundo = await Mundo.findOrFail( dado.mundo_id );
      dado.mundo = await transform.item(mundo, TransformerMundo);

      var user = await User.findOrFail( dado.user_id );
      dado.user = await transform.item(user, TransformerUser);

      if ( dado.professor_id ) {
        var professor = await Professor.findOrFail( dado.professor_id );
        dado.professor = await transform.item(professor, TransformerProfessor);
      }

      return response.status(201).send(dado)



    } catch (error) {
      
      response.status(400).send({
        "errors": [
          {
            "message": "Erro ao processar a sua solicitação! " + error,
            "field": "cpf",
            "validation": "cpf"
          }
        ]
      })

    }
    
  }

/**
  * @swagger
  * /admin/jogos/{id}:
  *   put:
  *     tags:
  *       - Jogo
  *     summary: Alterar de jogos
  *     description: Alterar dados cadastrais;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         description: Pode ser alterado o nome e/ou ativo
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/JogoSave"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Jogo"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async update ({ params: { id }, request, response, transform }) {

    var { mundo_id, nome, posicao, descricao, json, ativo, user_id, professor_id, grupo, tipo, items } = request.all();

    var dado = await Jogo.findOrFail( id );
    dado.merge({ mundo_id, nome, posicao, descricao, json, ativo, user_id, professor_id, grupo, tipo });

    await dado.save();

    var service = new Service();
    var itemsRet = await service.salvarItems( dado.id, items )

    dado.items = itemsRet;

    var mundo = await Mundo.findOrFail( dado.mundo_id );
    dado.mundo = await transform.item(mundo, TransformerMundo);

    var user = await User.findOrFail( dado.user_id );
    dado.user = await transform.item(user, TransformerUser);

    if ( dado.professor_id ) {
      var professor = await Professor.findOrFail( dado.professor_id );
      dado.professor = await transform.item(professor, TransformerProfessor);
    }

    return response.send(dado)
    
  }

/**
  * @swagger
  * /admin/jogos/{id}:
  *   delete:
  *     tags:
  *       - Jogo
  *     summary: Excluir jogo
  *     description: Excluir registro de jogo;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       204:
  *         description: Requisição finalizada
  *       404:
  *         description: Registro não encontrado
*/
  async delete ({ params: { id }, request, response, transform }) {

    var dado = await Jogo.findOrFail( id );
    
    await dado.delete();

    return response.status(204).send()
    
  }

}

module.exports = JogoController
