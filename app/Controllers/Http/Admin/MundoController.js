'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Mundo = use('App/Models/Mundo')
const Cenario = use('App/Models/Cenario')
const Transformer = use('App/Transformers/Admin/MundoTransformer')
const TransformerCenario = use('App/Transformers/Admin/CenarioTransformer')
const Env = use('Env')

class MundoController {
  
/**
* @swagger
* /admin/mundos:
*   get:
*     tags:
*       - Mundo
*     summary: Lista de mundo
*     description: Lista de mundos paginado;
*     produces:
*       - application/json
*     parameters:
*       - name: Authorization
*         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
*         in:  header
*         required: true
*         type: string
*       - name: "page"
*         in: "query"
*         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
*         type: "integer"
*       - name: "perPage"
*         in: "query"
*         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
*         type: "integer"
*       - name: "filtro"
*         in: "query"
*         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
*         type: "string"
*     responses:
*       200:
*         description: Retorno das informações
*         schema:
*           type: object
*           properties:
*             pagination:
*               type: object
*               properties:
*                 total:
*                   type: integer
*                 perPage:
*                   type: integer
*                 page:
*                   type: integer
*                 lastPage:
*                   type: integer
*             data:
*               type: array
*               items:
*                  type: object
*                  $ref: "#/definitions/Mundo"
*/
async index ({ request, response, transform, pagination }) {

  var queries = request.all();

  var dados;

  var query = Mundo.query()

  if ( queries.filtro ) {

    query.where('nome', 'ILIKE', `%${queries.filtro}%`)
    
  }

  query.orderBy('posicao', 'asc')

  var dados;

  if ( !queries.no_paginate ) {

    dados = await query.paginate( pagination.page, pagination.limit )
    dados = await transform.paginate(dados, Transformer)

  } else {
    dados = await query.select(['id', 'nome']).fetch();
    dados = await transform.collection(dados, Transformer)

    return response.send(dados) 

  }

  if ( dados.data.length != 0 ) {
    var idsEsc = [];

    dados.data.forEach(al => {
      idsEsc.push( al.cenario_id );
    });

    var cenarios = await Cenario.query().whereIn('id', idsEsc).fetch();
    cenarios = await transform.collection( cenarios, TransformerCenario )

    dados.data.forEach(al => {
      cenarios.forEach(e => {
        if ( al.cenario_id == e.id ) {
          al.cenario = e;
        }
      });
    });
  }

  return response.send(dados)

}
  
/**
  * @swagger
  * /admin/mundos/{id}:
  *   get:
  *     tags:
  *       - Mundo
  *     summary: Detalhe de mundos
  *     description: Detalhe de mundos;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Mundo"
  *       404:
  *         description: Registro não encontrado
  * components:
  *   schemas:
  *     Mundo:
  *       type: Mundo
*/
async show ({ params: { id }, request, response, transform }) {

    var dados = await Mundo.findOrFail(id)
    //busca a ligação entre mundo e cenarios
    var cenario = await Cenario.findOrFail( dados.cenario_id );

    var urlImageBase = 'http://' + Env.get('HOST') + ':' + Env.get('PORT') + '/uploads/mundos/' + id + '/';
    dados.image_base_64 = urlImageBase + dados.image_base_64;
    dados.image_base_64_final = urlImageBase + dados.image_base_64_final;
    dados.thumb_base_64 = urlImageBase + dados.thumb_base_64;
    dados.thumb_base_64_final = urlImageBase + dados.thumb_base_64_final;

    dados = await transform.item(dados, Transformer)

    dados.cenario = await transform.item(cenario, TransformerCenario);

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/mundos:
  *   post:
  *     tags:
  *       - Mundo
  *     summary: Salvar de mundos
  *     description: Salvar de mundos;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/AlunoSave"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Mundo"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, transform }) {

    try {
      
      const { cenario_id, nome, posicao, url_image_base, ativo, descricao } = request.all()

      var dado = await Mundo.create( { cenario_id, nome, posicao, url_image_base, ativo, descricao })

      dado = await transform.item(dado, Transformer)

      var cenario = await Cenario.findOrFail( dado.cenario_id );

      dado.cenario = await transform.item(cenario, TransformerCenario);

      return response.status(201).send(dado)



    } catch (error) {
      
      response.status(400).send({
        "errors": [
          {
            "message": "Erro ao processar a sua solicitação! " + error,
            "field": "cpf",
            "validation": "cpf"
          }
        ]
      })

    }
    
  }

async saveImage ({ params: { id }, request, response, transform }) {

  try {
    
    const { image_base_64, image_base_64_final, thumb_base_64, thumb_base_64_final} = request.all()

    var dados = await Mundo.findOrFail( id );
    dados.merge({ image_base_64, image_base_64_final, thumb_base_64, thumb_base_64_final });

    await dados.save();
    
    return response.status(201).send({'ok': true})



  } catch (error) {
    
    response.status(400).send({
      "errors": [
        {
          "message": "Erro ao processar a sua solicitação! " + error,
          "field": "cpf",
          "validation": "cpf"
        }
      ]
    })

  }
  
}

/**
  * @swagger
  * /admin/mundos/{id}:
  *   put:
  *     tags:
  *       - Mundo
  *     summary: Alterar de mundos
  *     description: Alterar dados cadastrais;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         description: Pode ser alterado o nome e/ou ativo
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/AlunoSave"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Mundo"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async update ({ params: { id }, request, response, transform }) {

    var { cenario_id, nome, posicao, url_image_base, ativo, descricao } = request.all();

    var dados = await Mundo.findOrFail( id );
    dados.merge({ cenario_id, nome, posicao, url_image_base, ativo, descricao });

    await dados.save();

    var cenario = await Cenario.findOrFail( dados.cenario_id );

    dados = await transform.item(dados, Transformer)

    dados.cenario = await transform.item(cenario, TransformerCenario);

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/mundos/{id}:
  *   delete:
  *     tags:
  *       - Mundo
  *     summary: Excluir mundo
  *     description: Excluir registro de mundo;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       204:
  *         description: Requisição finalizada
  *       404:
  *         description: Registro não encontrado
*/
  async delete ({ params: { id }, request, response, transform }) {

    var dado = await Mundo.findOrFail( id );
    
    await dado.delete();

    return response.status(204).send()
    
  }

}

module.exports = MundoController
