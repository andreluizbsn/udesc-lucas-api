'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with beers
 */
const Professor = use('App/Models/Professor')
const Database = use('Database')
const ProfessorEscola = use('App/Models/ProfessorEscola')
const Escola = use('App/Models/Escola')
const Transformer = use('App/Transformers/Admin/ProfessorTransformer')
const TransformerEscola = use('App/Transformers/Admin/EscolaTransformer')

class ProfessorController {
  
  /**
  * @swagger
  * /admin/professores:
  *   get:
  *     tags:
  *       - Professor
  *     summary: Lista de professor
  *     description: Lista de professores paginado;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token. Vide autenticação;
  *         in:  header
  *         required: true
  *         type: string
  *       - name: "page"
  *         in: "query"
  *         description: "Número da página. Se não informado, o sistema assume automaticamente 1"
  *         type: "integer"
  *       - name: "perPage"
  *         in: "query"
  *         description: "Número de registros por página. Caso não informar, o sistema assume automaticamente 10"
  *         type: "integer"
  *       - name: "filtro"
  *         in: "query"
  *         description: "O filtro irá buscar nomes que contenham o filtro aplicado"
  *         type: "string"
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           type: object
  *           properties:
  *             pagination:
  *               type: object
  *               properties:
  *                 total:
  *                   type: integer
  *                 perPage:
  *                   type: integer
  *                 page:
  *                   type: integer
  *                 lastPage:
  *                   type: integer
  *             data:
  *               type: array
  *               items:
  *                  type: object
  *                  $ref: "#/definitions/Professor"
*/
  async index ({ request, response, transform, pagination }) {

    var queries = request.all();

    var dados;

    var query = Professor.query()

    if ( queries.filtro ) {

      query.where('nome', 'ILIKE', `%${queries.filtro}%`)
      
    }

    query.orderBy('nome', 'asc')

    var dados;

    if ( !queries.no_paginate ) {

      var dados = await query.paginate( pagination.page, pagination.limit )

      dados = await transform.paginate(dados, Transformer)
    } else {
      dados = await query.fetch()

      dados = await transform.collection(dados, Transformer)
    }

    return response.send(dados)

  }
  
/**
  * @swagger
  * /admin/professores/{id}:
  *   get:
  *     tags:
  *       - Professor
  *     summary: Detalhe de professores
  *     description: Detalhe de professores;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Professor"
  *       404:
  *         description: Registro não encontrado
  * components:
  *   schemas:
  *     Professor:
  *       type: Professor
*/
async show ({ params: { id }, request, response, transform }) {

    var dados = await Professor.findOrFail(id)
    //busca a ligação entre professor e escolas
    var dadosLigacao = await ProfessorEscola.query().where('professor_id', dados.id).fetch();

    var escIDs = []

    dadosLigacao.rows.forEach(l => {
      escIDs.push( l.escola_id );
    });

    var escolas = await Escola.query().whereIn('id', escIDs).fetch();
    
    dados.escolas = escolas;

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/professores:
  *   post:
  *     tags:
  *       - Professor
  *     summary: Salvar de professores
  *     description: Salvar de professores;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/Professor"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Professor"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async save ({ request, response, transform }) {

    try {
      
      const { nome, email, area, escolas } = request.all()

      /*if ( cpf ) {
        if ( cpf.length != 11 ) {
          response.status(400).send(
            {
              "errors": [
                {
                  "message": "O CPF deve ter 11 caracteres!",
                  "field": "cpf",
                  "validation": "cpf"
                }
              ]
            }
          )  
          return;
        } else if ( isNaN( cpf ) ) { 
          response.status(400).send(
            {
              "errors": [
                {
                  "message": "O CPF deve conter apenas números!",
                  "field": "cpf",
                  "validation": "cpf"
                }
              ]
            })  
          return;
        }
      }*/

      var dado = await Professor.create( { nome, email, area })

      dado = await transform.item(dado, Transformer)

      if ( Array.isArray(escolas) ) {
        var dadosInserir = [];
        escolas.forEach(e => {
          dadosInserir.push({
            'professor_id': dado.id,
            'escola_id': e
          })
        });
        await ProfessorEscola.createMany(dadosInserir);
      }

      return response.status(201).send(dado)



    } catch (error) {
      
      response.status(400).send({
        message: "Erro ao processar a sua solicitação!"
      })

    }
    
  }

/**
  * @swagger
  * /admin/professores/{id}:
  *   put:
  *     tags:
  *       - Professor
  *     summary: Alterar de professores
  *     description: Alterar dados cadastrais;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *       - name: Body
  *         description: Pode ser alterado o nome e/ou ativo
  *         in:  body
  *         schema:
  *           $ref: "#/definitions/Professor"
  *     responses:
  *       201:
  *         description: Retorno das informações
  *         schema:
  *           $ref: "#/definitions/Professor"
  *       401:
  *         description: Validação Geral
  *         example: {
  *            "errors": [
  *              {
  *                "message": (string),
  *                "field": (string),
  *                "validation": "cpf"
  *              }
  *            ]
  *          }
*/
async update ({ params: { id }, request, response, transform }) {

    var { nome, email, area, escolas } = request.all();

    /*if ( cpf ) {
      if ( cpf.length != 11 ) {
        response.status(400).send(
          {
            "errors": [
              {
                "message": "O CPF deve ter 11 caracteres!",
                "field": "cpf",
                "validation": "cpf"
              }
            ]
          }
        )  
        return;
      } else if ( isNaN( cpf ) ) { 
        response.status(400).send(
          {
            "errors": [
              {
                "message": "O CPF deve conter apenas números!",
                "field": "cpf",
                "validation": "cpf"
              }
            ]
          })  
        return;
      } else {
        var exdado = await Professor.query()
        .where('cpf', cpf)
        .fetch();

        if ( exdado.rows.length != 0 ) {
          var otherdado = false;
          exdado.rows.forEach(d => {
            if ( d.id != id ) {
              otherdado = true;
            }
          });
          if ( otherdado ) {
            response.status(400).send(
              {
                "errors": [
                  {
                    "message": "O CPF já está sendo usado em outro cadastro!",
                    "field": "cpf",
                    "validation": "cpf"
                  }
                ]
              })  
            return;
          }
        }
      } 
    }*/

    var exdado = await Professor.query()
    .where('email', email)
    .fetch();

    var hasOther = false;
    exdado.rows.forEach(p => {
      if ( p.email == email && p.id != id ) {
        hasOther = true;
      }
    });

    if ( hasOther ) {
      response.status(400).send(
        {
          "errors": [
            {
              "message": "O E-mail já está sendo usado em outro cadastro!",
              "field": "email",
              "validation": "email"
            }
          ]
        })  
      return;
    }

    var dados = await Professor.findOrFail( id );
    dados.merge({ nome, email, area });

    if ( Array.isArray(escolas) ) {

      if ( escolas.length == 0 ) {
        //elimina todos os registros de ligação
        await ProfessorEscola.query().where('professor_id', id).delete();
      } else {
        var profsEscolas = await ProfessorEscola.query().where('professor_id', id).fetch();

        var hasReg = false;

        var dadosInserir = [];

        //determinar o que vai ser inserido
        escolas.forEach(e => {
          profsEscolas.rows.forEach(p => {
            if ( p.escola_id == e ) {
              hasReg = true;
            }
          });

          if ( !hasReg ) {
            dadosInserir.push({
              'escola_id': e,
              'professor_id': id
            })
          }
          hasReg = false;
        });

        if ( dadosInserir.length != 0 ) {
          await ProfessorEscola.createMany(dadosInserir)
          profsEscolas = await ProfessorEscola.query().where('professor_id', id).fetch();
        }

        var dadosEliminar = [];

        //determinar o que vai ser eliminado
        profsEscolas.rows.forEach(p => {
          escolas.forEach(e => {
            if ( p.escola_id == e ) {
              hasReg = true;
            }
          });

          if ( !hasReg ) {
            dadosEliminar.push(p.id)
          }
          hasReg = false;
        });

        if ( dadosEliminar.length != 0 ) {
          
          await ProfessorEscola.query().whereIn('id', dadosEliminar).delete();

        }

      }

    }

    await dados.save();

    return response.send(dados)
    
  }

/**
  * @swagger
  * /admin/professores/{id}:
  *   delete:
  *     tags:
  *       - Professor
  *     summary: Excluir professor
  *     description: Excluir registro de professor;
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: Authorization
  *         description: Autorização do sistema. Composto por 'Bearer ' + token
  *         in:  header
  *         required: true
  *         type: string
  *     responses:
  *       204:
  *         description: Requisição finalizada
  *       404:
  *         description: Registro não encontrado
*/
  async delete ({ params: { id }, request, response, transform }) {

    var dado = await Professor.findOrFail( id );

    await ProfessorEscola.query().where('professor_id', id).delete();
    
    await dado.delete();

    return response.status(204).send()
    
  }

}

module.exports = ProfessorController
