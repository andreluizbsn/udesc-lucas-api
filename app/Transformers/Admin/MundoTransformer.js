'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class MundoTransformer extends BumblebeeTransformer {

  transform (model) {
    return {
      id: model.id,
      nome: model.nome,
      posicao: model.posicao,
      url_image_base: model.url_image_base,
      image_base_64: model.image_base_64,
      image_base_64_final: model.image_base_64_final,
      thumb_base_64: model.thumb_base_64,
      thumb_base_64_final: model.thumb_base_64_final,
      descricao: model.descricao,
      ativo: model.ativo,
      cenario_id: model.cenario_id
    }
  }

}

module.exports = MundoTransformer
