'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class CenarioTransformer extends BumblebeeTransformer {

  transform (model) {
    return {
      id: model.id,
      nome: model.nome,
      image_logo: model.image_logo,
      image_backgound: model.image_backgound,
      ativo: model.ativo
    }
  }

}

module.exports = CenarioTransformer
