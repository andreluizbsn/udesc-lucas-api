'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class DicionarioTransformer extends BumblebeeTransformer {

  transform (model) {
    return {
      id: model.id,
      chave: model.chave,
      valor: model.valor
    }
  }

}

module.exports = DicionarioTransformer
