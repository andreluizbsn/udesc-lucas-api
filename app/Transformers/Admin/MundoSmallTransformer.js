'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class MundoSmallTransformer extends BumblebeeTransformer {

  transform (model) {
    return {
      id: model.id,
      nome: model.nome
    }
  }

}

module.exports = MundoSmallTransformer
