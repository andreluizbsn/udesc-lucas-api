'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class EscolaTransformer extends BumblebeeTransformer {
  
  transform (model) {
    return {
      id: model.id,
      nome: model.nome,
      cnpj: model.cnpj,
      sigla: model.sigla
    }
  }

}

module.exports = EscolaTransformer
