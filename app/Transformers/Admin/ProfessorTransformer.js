'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class ProfessorTransformer extends BumblebeeTransformer {
  
  transform (model) {
    return {
      id: model.id,
      nome: model.nome,
      email: model.email,
      area: model.area
    }
  }

}

module.exports = ProfessorTransformer
