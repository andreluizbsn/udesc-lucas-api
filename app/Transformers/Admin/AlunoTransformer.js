'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class AlunoTransformer extends BumblebeeTransformer {

  transform (model) {
    return {
      id: model.id,
      nome: model.nome,
      email: model.email,
      serie: model.serie,
      escola_id: model.escola_id
    }
  }

}

module.exports = AlunoTransformer
