'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class UserTransformer extends BumblebeeTransformer {
  
  transform (model) {
    return {
      id: model.id,
      nome: model.nome,
      login: model.email,
      tipo: model.tipo,
      ativo: model.ativo,
      aluno_id: model.aluno_id,
      professor_id: model.professor_id,
      created_at: model.created_at,
      updated_at: model.updated_at
    }
  }

}

module.exports = UserTransformer
