'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class JogoTransformer extends BumblebeeTransformer {

  transform (model) {
    return {
      id: model.id,
      nome: model.nome,
      posicao: model.posicao,
      descricao: model.descricao,
      json: model.json,
      ativo: model.ativo,
      user_id: model.user_id,
      professor_id: model.professor_id,
      mundo_id: model.mundo_id,
      grupo: model.grupo,
      tipo: model.tipo
    }
  }

}

module.exports = JogoTransformer
