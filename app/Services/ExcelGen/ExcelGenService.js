var Excel = use('excel4node');
const Env = use('Env')

class ExcelGenService {

    get_data_report() {

        var date = new Date();
        var retorno = '';

        retorno = date.getFullYear().toString() + '-';
        if ( date.getMonth() + 1 < 10 ) {
            retorno += '0';
        }
        retorno += (date.getMonth() + 1).toString() + '-';

        if ( date.getDate() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getDate() + 1).toString() + '_';

        if ( date.getHours() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getHours() + 1).toString() + '-';

        if ( date.getMinutes() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getMinutes() + 1).toString() + '-';

        if ( date.getSeconds() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getSeconds() + 1).toString();

        return retorno;

    }

    async exec( data ) {

        /*
        data = [
            {
                title: SheetName,
                header: [
                    {
                        name: 'col1',
                        type: 'string'
                    },
                    {
                        name: 'col2',
                        type: 'string'
                    },
                    {
                        name: 'coln',
                        type: 'number'
                    }
                ],
                content: [
                    {
                        col1: 123
                        col2: '456'
                        coln: 78.9
                    }
                ]
            }
        ]
        */

        var workbook = new Excel.Workbook();
        var worksheet;
        var style = workbook.createStyle({
        font: {
            color: '#000000',
            size: 12
        },
            numberFormat: 'R$#,##0.00; (R$#,##0.00); -'
        });
        var line = 2;
        var cell = 1;

        data.forEach(d => {
            worksheet = workbook.addWorksheet(d.title); 
            cell = 1;
            line = 2;
            d.header.forEach(h => {
                worksheet.cell(1,cell).string(h.name).style(style);
                cell+=1;
            });
            cell = 1;
            d.content.forEach(c => {
                cell = 1;
                d.header.forEach(h => {
                    if(h.type == 'string'){
                        worksheet.cell(line,cell).string(c[h.name]).style(style);
                    }else if(h.type == 'number'){
                        worksheet.cell(line,cell).number(parseFloat(c[h.name])).style(style);
                    }
                    //worksheet.cell(line,cell).string(c[h].toString()).style(style);
                    cell+=1;
                });
                line += 1;
            });
        });

        var fileName = 'relatorio_' + this.get_data_report() + '.xlsx';

        workbook.write('./public/excel/' + fileName);

        var url = Env.get('APP_URL', 'http://localhost:3334') + '/excel/' + fileName;

        return url;


    }

}

module.exports = ExcelGenService