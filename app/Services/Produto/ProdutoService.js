'use strict'

const Produto = use('App/Models/Produto')
const Lancamento = use('App/Models/Lancamento')
const Database = use('Database');

class ProdutoService {

    async atualizarPosicaoEstoque(produto_id, quantidade, entrada, valor_unitario) {

        var dado = await Produto.findOrFail( produto_id );

        var estoque_atual = parseFloat(dado.estoque_atual) || 0;

        estoque_atual = parseFloat(estoque_atual) + ( entrada ? parseFloat(quantidade) : (parseFloat(quantidade)*-1) );

        dado.estoque_atual = estoque_atual;
        dado.valor_unitario_atual = valor_unitario;

        await dado.save();

    }

    async atualizarPosicaoEstoqueForcar(produto_id, categoria_id) {

        var dados;

        var dadosQr = Produto.query();

        if ( produto_id ) {
            var prodsArr = []
            if ( typeof produto_id == 'string' ) {
                prodsArr = produto_id.split('-')
            } else {
                prodsArr.push(produto_id);
            }
            dadosQr.whereIn('id', prodsArr)
        }

        if ( categoria_id ) {
            var catsArr = categoria_id.split('-')
            dadosQr.whereIn('categoria_id', catsArr)
        }

        var dados = await dadosQr.fetch();

        var produtosIDs = []

        dados.rows.forEach(pro => {
            produtosIDs.push( pro.id );
        });

        var lancamentos = await Lancamento.query().whereIn('produto_id', produtosIDs).orderBy('data_lancamento', 'asc').fetch();

        var arrSql = [];
        var sql = '';
        var estoque_atual = 0;
        var valor_unitario_atual = 0;

        dados.rows.forEach(pro => {
            lancamentos.rows.forEach(l => {
                if ( l.produto_id == pro.id ) {
                    if ( l.entrada ) {
                        estoque_atual += parseFloat( l.quantidade );
                        valor_unitario_atual = parseFloat( l.valor_unitario );
                    } else {
                        estoque_atual -= parseFloat( l.quantidade );
                    }
                }
            });

            sql = 'update produtos set estoque_atual = ' + estoque_atual + ', valor_unitario_atual = ' + valor_unitario_atual + ' where id = ' + pro.id;

            arrSql.push( sql );

            estoque_atual = 0;
            valor_unitario_atual = 0;
            sql = '';
        });


        const promises = arrSql.map(async sql => {

            await Database.raw( sql );

        });
    
        await Promise.all(promises);
    }
}

module.exports = ProdutoService