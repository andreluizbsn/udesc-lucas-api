'use strict'

const Score = use('App/Models/Score')
const Database = use('Database')

class ScoreService {

    async syncData( user_id, data ) {

        var retorno = {
            'inserts': 0,
            'updates': 0
        }

        var dadosFormatadosInserir = [];
        var dadoFormatadoInserir = new Object();
        var dadosFormatadosAlterar = [];
        var dadoFormatadoAlterar = new Object();
        var scores = await Score.query().where('user_id', user_id).fetch();
        var hasReg = false;

        data.forEach(d => {
            d.data.forEach(dt => {
                hasReg = false;
                scores.rows.forEach(sc => {
                    if ( sc.local_id == dt.id && sc.local == d.local ) {
                        hasReg = true;
                        dadoFormatadoAlterar = new Object();
                        dadoFormatadoAlterar.user_id = user_id;
                        dadoFormatadoAlterar.local = d.local;
                        dadoFormatadoAlterar.local_id = dt.id;
                        dadoFormatadoAlterar.id = sc.id;
                        dadoFormatadoAlterar.tentativas = dt.tentativas;
                        dadoFormatadoAlterar.finalizado = dt.finalizado;
                        dadosFormatadosAlterar.push(dadoFormatadoAlterar);
                    }
                });
                if ( !hasReg ) {
                    dadoFormatadoInserir = new Object();
                    dadoFormatadoInserir.user_id = user_id;
                    dadoFormatadoInserir.local = d.local;
                    dadoFormatadoInserir.local_id = dt.id;
                    dadoFormatadoInserir.tentativas = dt.tentativas;
                    dadoFormatadoInserir.finalizado = dt.finalizado;
                    dadosFormatadosInserir.push(dadoFormatadoInserir);
                }
            });
        });

        retorno.inserts = dadosFormatadosInserir.length;
        retorno.updates = dadosFormatadosAlterar.length;

        if (retorno.inserts > 0) {
            await this.insertScores(dadosFormatadosInserir);
        }
        if (retorno.updates > 0) {
            await this.updateScores(dadosFormatadosAlterar);
        }

        return retorno;

    }

    async insertScores( data ) {
        await Database.table('scores').insert(data)
    }

    async updateScores( data ) {
        var sqls = [];
        data.forEach(d => {
            sqls.push('update scores set tentativas = ' + d.tentativas + ', finalizado = ' + d.finalizado + ' where id = ' + d.id + ';');

        });

        await Database.raw(sqls.join(''));
    }

    async getAppData( user_id ) {

        var dados = await Score.query()
        .where('user_id', user_id)
        .fetch();

        var retorno = await this.formatAppData(dados.rows);

        return retorno;
    }

    async formatAppData( data ) {

        console.log(data);

        var retorno = [];
        var hasreg = false;
        data.forEach(dt => {
            hasreg = false;
            retorno.forEach(ret => {
                if ( ret.local == dt.local ) {
                    hasreg = true;
                    ret.data.push({
                        "id": dt.local_id,
					    "tentativas": dt.tentativas,
					    "finalizado": dt.finalizado
                    })
                }
            });
            if ( !hasreg ) {
                retorno.push({
                    'local': dt.local,
                    'data': [{
                                "id": dt.local_id,
                                "tentativas": dt.tentativas,
                                "finalizado": dt.finalizado
                            }]
                })
            }
        });

        return retorno;
    }

}

module.exports = ScoreService