'use strict'

const fs = require("fs");
const path = require("path");
const puppeteer = require('puppeteer');
const handlebars = require("handlebars");
const Env = use('Env')

class PdfGenService {

    get_data_report() {

        var date = new Date();
        var retorno = '';

        retorno = date.getFullYear().toString() + '-';
        if ( date.getMonth() + 1 < 10 ) {
            retorno += '0';
        }
        retorno += (date.getMonth() + 1).toString() + '-';

        if ( date.getDate() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getDate() + 1).toString() + '_';

        if ( date.getHours() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getHours() + 1).toString() + '-';

        if ( date.getMinutes() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getMinutes() + 1).toString() + '-';

        if ( date.getSeconds() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getSeconds() + 1).toString();

        return retorno;

    }

    get_data_string( date ) {

        var retorno = '';

        if ( date.getDate() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getDate() + 1).toString() + '/';

        if ( date.getMonth() + 1 < 10 ) {
            retorno += '0';
        }
        retorno += (date.getMonth() + 1).toString() + '/';

        retorno += date.getFullYear().toString() + ' ';
        
        if ( date.getHours() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getHours() + 1).toString() + ':';

        if ( date.getMinutes() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getMinutes() + 1).toString() + ':';

        if ( date.getSeconds() < 10 ) {
            retorno += '0';
        }
        retorno += (date.getSeconds() + 1).toString();

        return retorno;
    }

    convert_float_currency( val ) {

        if ( !val ) {
            return;
        }

        var valarr = val.split('.');
        var retorno = 'R$ ' + valarr[0];
        if ( valarr.length == 2 ) {

            var lval = valarr[1].length;

            if ( valarr[1].length == 2 ) {
                retorno += ',' + valarr[1];    
            } else if ( valarr[1].length == 1 ) {
                retorno += ',' + valarr[1] + '0';    
            } else if ( valarr[1].length > 2 ) {
                retorno += ',' + valarr[1].substring(0,2);
            }

        } else {
            retorno += ',00';
        }

        return retorno;
    }

    convert_data_table( header, data ) {
        var retorno = '<table><thead>';

        header.forEach(h => {
            retorno += '<th>' + h.f_name + '</th>';
        });

        retorno += '<thead><tbody>';

        data.forEach(c => {
            retorno += '<tr>';
            header.forEach(h => {
                if(h.pdfformat == 'string'){
                    retorno += '<td>'+c[h.name]+'</td>';
                }else if(h.pdfformat == 'number'){
                    retorno += '<td>'+parseFloat(c[h.name])+'</td>';
                }else if(h.pdfformat == 'currency'){
                    retorno += '<td>'+this.convert_float_currency(c[h.name].toString())+'</td>';
                }else{
                    retorno += '<td>'+c[h.name]+'</td>';
                }
            });
            retorno += '</tr>';
        });

        retorno += '</tbody></table><strong>'+data.length+' registros</strong>';

        return retorno;

    }

    async gerar(templatename, data, filters) {

        var tabela_dados = '';

        data.forEach(d => {
            if ( d.pdftype == 'group1' ) {
                tabela_dados += '<h3>' + d.title_pre + d.title + '</h3>';
                tabela_dados += this.convert_data_table( d.header, d.content );
            }
        });

        data = {
            date: this.get_data_string( new Date() ),
            filters: filters
        }

        console.log(process.cwd());
        var templateHtml = fs.readFileSync(path.join(process.cwd(), 'app/report-templates/'+templatename+'.html'), 'utf8');

        templateHtml = templateHtml.replace("__tabela_dados__", tabela_dados);

        var template = handlebars.compile(templateHtml);
        var html = template(data);

        var milis = new Date();
        milis = milis.getTime();

        //var filename = `${data.name}-${milis}.pdf`;
        var filename = templatename + '-' + this.get_data_report() + '.pdf';

        var pdfPath = path.join('./public/pdf', filename);

        var options = {
            width: '1230px',
            headerTemplate: "<p></p>",
            footerTemplate: "<p></p>",
            displayHeaderFooter: false,
            margin: {
                top: "0px",
                bottom: "0px"
            },
            printBackground: true,
            path: pdfPath
        }

        const browser = await puppeteer.launch({
            args: ['--no-sandbox'],
            headless: true
        });

        var page = await browser.newPage();
        
        await page.goto(`data:text/html;charset=UTF-8,${html}`, {
            waitUntil: 'networkidle0'
        });

        await page.pdf(options);
        await browser.close();

        var url = Env.get('APP_URL', 'http://localhost:3334') + '/pdf/' + filename;

        var retorno = {
            'url': url,
            'name': filename
        }

        return retorno;

    }

}

module.exports = PdfGenService