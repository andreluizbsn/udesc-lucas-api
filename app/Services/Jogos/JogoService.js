const JogoItem = use('App/Models/JogoItem')
const Database = use('Database')


class JogoService {

    async salvarItems( jogo_id, items ) {

        var retorno = [];

        var itemsInsert = [];
        var sqlItemsUpdate = "";
        var idDelete = [];

        var itemsAtuais = await JogoItem.query().where('jogo_id', jogo_id).fetch();

        if ( itemsAtuais.rows.length != 0 ) { 
            var hasReg = false;
            itemsAtuais.rows.forEach(a => {
                items.forEach(i => {
                    if ( i.id == a.id ) {
                        hasReg = true;
                    }
                });

                if ( !hasReg ) {
                    idDelete.push( a.id );
                }
            });
        }

        if ( idDelete.length != 0 ) {
            await JogoItem.query()
            .whereIn('id', idDelete)
            .delete();
        }

        items.forEach(i => {
            //update
            if ( i.id > 0 ) {
                sqlItemsUpdate += "update jogo_items set dado = '" + i.dado + "', verdadeiro = " + i.verdadeiro + " where id = " + i.id + ";"
            }
            //insert 
            else {
                itemsInsert.push({
                    'jogo_id': jogo_id,
                    'dado': i.dado,
                    'verdadeiro': i.verdadeiro
                })
            }
        });

        if ( itemsInsert.length != 0 ) {
            await JogoItem.createMany(itemsInsert);
        }

        if ( sqlItemsUpdate != '' ) {
            await Database.raw( sqlItemsUpdate );
        }

        var retornoPrev = await JogoItem.query().where('jogo_id', jogo_id).fetch();

        if ( retornoPrev.rows.length != 0 ) {
            retorno = retornoPrev.rows;
        }

        return retorno;
    }

}

module.exports = JogoService