'use strict'

class StringService {

    async convertToUrl( titulo ) {

        var url = titulo.toLowerCase()
        .split(' ').join('_')
        .split('á').join('a')
        .split('à').join('a')
        .split('â').join('a')
        .split('ã').join('a')
        .split('ä').join('a')
        .split('é').join('e')
        .split('è').join('e')
        .split('ê').join('e')
        .split('ë').join('e')
        .split('í').join('i')
        .split('ì').join('i')
        .split('î').join('i')
        .split('ï').join('i')
        .split('ó').join('o')
        .split('ò').join('o')
        .split('ô').join('o')
        .split('õ').join('o')
        .split('ö').join('o')
        .split('ú').join('u')
        .split('ù').join('u')
        .split('û').join('u')
        .split('ü').join('u')
        .split('ç').join('c')
        .split('&').join('e');

        return url;

    }
}

module.exports = StringService