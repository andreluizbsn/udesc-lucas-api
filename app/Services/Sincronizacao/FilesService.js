'use strict'

var http = require('http');
var fs = require('fs');
const Database = use('Database');

class FilesService {

    fileNameGen ( length ) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    async execute( empresa_id, modelo, id, url, formato, nomeArquivo ) {

        //var nomeArquivo = await this.fileNameGen( 50 ) + formato;
        var diretorio = './public/uploads/' + empresa_id + '/'  + modelo + '/' + id;

        console.log(diretorio);
        console.log(nomeArquivo);


        fs.mkdirSync(diretorio, { recursive: true });
        var file = fs.createWriteStream(diretorio + '/' + nomeArquivo);
        var request = http.get(url, function(response) {
            response.pipe(file);
            file.on('finish', function() {
                file.close();  // close() is async, call cb after close completes.
            });
        }).on('error', function(err) { // Handle errors
            fs.unlink(dest); // Delete the file async. (But we don't check the result)
            //if (cb) cb(err.message);
        });

        return nomeArquivo;
  
    }

    async delete ( modelo, id, nomeArquivo ) {

        var diretorio = './public/uploads/'  + modelo + '/' + id;

        fs.unlink(diretorio + '/' + nomeArquivo, (err) => {
            if (err) {
                console.error(err)
                return
            }
        })
    }

    async saveBase64( modelo, id, arquivo, formato ) {

        var diretorio = './public/uploads/'  + modelo + '/' + id + '/';
        var arquivoSistema = this.fileNameGen( 50 ) + formato;

        fs.mkdirSync(diretorio, { recursive: true });

        diretorio += arquivoSistema;

        try
        {
          await fs.writeFile(diretorio, arquivo, 'base64', function() {

          });
        }
        catch(error)
        {
            console.log('ERROR:', error);
        }

        return arquivoSistema;
    }
}

module.exports = FilesService