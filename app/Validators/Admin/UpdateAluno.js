'use strict'

class AdminUpdateAluno {
  get rules () {
    return {
      nome: 'required',
      email: 'required', 
      serie: 'required', 
      escola_id: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'email.required': 'O E-mail é obrigatório',
      'serie.required': 'A Série é obrigatória',
      'escola_id.required': 'A Escola é obrigatória'
      
    }
  }
}

module.exports = AdminUpdateAluno
