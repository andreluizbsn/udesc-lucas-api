'use strict'

class AdminStoreProfessor {
  get rules () {
    return {
      nome: 'required',
      email: 'required|unique:professors,email', 
      area: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'email.required': 'O E-mail é obrigatório',
      'email.unique': 'Esse E-mail já está cadastrado',
      'area.required': 'A Área de atuação é obrigatória'
      
    }
  }
}

module.exports = AdminStoreProfessor
