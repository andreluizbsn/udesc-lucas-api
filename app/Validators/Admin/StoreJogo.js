'use strict'

class AdminStoreJogo {
  get rules () {
    return {
      nome: 'required',
      user_id: 'required',
      posicao: 'required',
      mundo_id: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'user_id.required': 'Um usuário é obrigatório',
      'posicao.required': 'A posição é obrigatória',
      'mundo_id.required': 'Um mundo precisa ser vinculado ao Jogo'
    }
  }
}

module.exports = AdminStoreJogo
