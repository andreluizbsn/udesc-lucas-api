'use strict'

class AdminUpdateEscola {
  get rules () {
    return {
      nome: 'required',
      cnpj: 'required', 
      sigla: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'cnpj.required': 'O CNPJ é obrigatório',
      'sigla.required': 'A Sigla é obrigatória'
      
    }
  }
}

module.exports = AdminUpdateEscola
