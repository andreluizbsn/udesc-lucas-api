'use strict'

class AdminUpdateProfessor {
  get rules () {
    return {
      nome: 'required',
      email: 'required', 
      area: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'email.required': 'O E-mail é obrigatório',
      'area.required': 'A Área é obrigatória'
      
    }
  }
}

module.exports = AdminUpdateProfessor
