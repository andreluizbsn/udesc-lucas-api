'use strict'

class AdminStoreEscola {
  get rules () {
    return {
      nome: 'required',
      cnpj: 'required|unique:escolas,cnpj', 
      sigla: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'cnpj.required': 'O CNPJ é obrigatório',
      'cnpj.unique': 'Esse CNPJ já está cadastrado',
      'sigla.required': 'A Sigla é obrigatória'
      
    }
  }
}

module.exports = AdminStoreEscola
