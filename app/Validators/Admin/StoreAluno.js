'use strict'

class AdminStoreAluno {
  get rules () {
    return {
      nome: 'required',
      email: 'required|unique:alunos,email', 
      serie: 'required', 
      escola_id: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'email.required': 'O E-mail é obrigatório',
      'email.unique': 'Esse E-mail já está cadastrado',
      'serie.required': 'A Série de atuação é obrigatória',
      'escola_id.required': 'A Escola de atuação é obrigatória'
      
    }
  }
}

module.exports = AdminStoreAluno
