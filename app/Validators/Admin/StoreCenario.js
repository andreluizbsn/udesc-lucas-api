'use strict'

class AdminStoreCenario {
  get rules () {
    return {
      nome: 'required',
      url_image_base: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'url_image_base.required': 'A imagem é obrigatória'      
    }
  }
}

module.exports = AdminStoreCenario
