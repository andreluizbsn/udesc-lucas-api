'use strict'

class AdminStoreMundo {
  get rules () {
    return {
      nome: 'required',
      url_image_base: 'required',
      posicao: 'required',
      cenario_id: 'required'
    }
  }

  get messages() {
    return {
      'nome.required': 'O nome é obrigatório',
      'url_image_base.required': 'A imagem é obrigatória',
      'posicao.required': 'A posição é obrigatória',
      'cenario_id.required': 'Um cenário precisa ser vinculado ao mundo'
    }
  }
}

module.exports = AdminStoreMundo
