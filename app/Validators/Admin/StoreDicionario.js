'use strict'

class AdminStoreDicionario {
  get rules () {
    return {
      chave: 'required',
      valor: 'required'
    }
  }

  get messages() {
    return {
      'chave.required': 'A chave é obrigatória',
      'valor.required': 'O valor é obrigatório'      
    }
  }
}

module.exports = AdminStoreDicionario
