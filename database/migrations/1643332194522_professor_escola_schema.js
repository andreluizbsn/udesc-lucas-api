'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProfessorEscolaSchema extends Schema {
  up () {
    this.create('professor_escolas', (table) => {
      table.increments()
      table.integer('professor_id').unsigned().references('id').inTable('professors')
      table.integer('escola_id').unsigned().references('id').inTable('escolas')
      table.timestamps()
    })
  }

  down () {
    this.drop('professor_escolas')
  }
}

module.exports = ProfessorEscolaSchema
