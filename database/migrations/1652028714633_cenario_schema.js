'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CenarioSchema extends Schema {
  up () {
    this.alter('cenarios', (table) => {
      table.text('image_base_64')    
    })
  }

  down () {
    this.drop('cenarios')
  }
}

module.exports = CenarioSchema
