'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JogoSchema extends Schema {
  up () {
    this.alter('jogos', (table) => {
      table.string('tipo', 50)
    })
  }

  down () {
    this.drop('jogos')
  }
}

module.exports = JogoSchema
