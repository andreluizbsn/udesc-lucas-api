'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataReleaseSchema extends Schema {
  up () {
    this.create('data_releases', (table) => {
      table.increments()
      table.integer('cenario').default(0)
      table.integer('mundo').default(0)
      table.integer('jogo').default(0)
      table.integer('jogo_item').default(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('data_releases')
  }
}

module.exports = DataReleaseSchema
