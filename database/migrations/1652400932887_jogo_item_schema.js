'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JogoItemSchema extends Schema {
  up () {
    this.create('jogo_items', (table) => {
      table.increments()
      table.integer('jogo_id').unsigned().references('id').inTable('jogos') 
      table.boolean('verdadeiro').defaultTo(true)
      table.text('dado')
      table.timestamps()
    })
  }

  down () {
    this.drop('jogo_items')
  }
}

module.exports = JogoItemSchema
