'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MundoSchema extends Schema {
  up () {
    this.alter('mundos', (table) => {
      table.text('image_base_64_final')    
      table.text('thumb_base_64')    
      table.text('thumb_base_64_final')
      table.string('descricao', 2000)
    })
  }

  down () {
    this.drop('mundos')
  }
}

module.exports = MundoSchema
