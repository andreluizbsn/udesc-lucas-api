'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JogoSchema extends Schema {
  up () {
    this.create('jogos', (table) => {
      table.increments()
      table.integer('mundo_id').unsigned().references('id').inTable('mundos')
      table.string('nome', 150)
      table.integer('posicao')
      table.string('descricao', 1000)
      table.string('json', 60000)  
      table.boolean('ativo').defaultTo(true)
      table.integer('user_id')
      table.integer('professor_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('jogos')
  }
}

module.exports = JogoSchema
