'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ScoreSchema extends Schema {
  up () {
    this.alter('scores', (table) => {
      table.integer('local_id')
    })
  }

  down () {
    this.drop('scores')
  }
}

module.exports = ScoreSchema
