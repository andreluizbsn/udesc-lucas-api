'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MundosSchema extends Schema {
  up () {
    this.create('mundos', (table) => {
      table.increments()
      table.integer('cenario_id').unsigned().references('id').inTable('cenarios')
      table.string('nome', 150)
      table.integer('posicao')
      table.string('url_image_base', 1000)      
      table.boolean('ativo').defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('mundos')
  }
}

module.exports = MundosSchema
