'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CenarioSchema extends Schema {
  up () {
    this.alter('cenarios', (table) => {
      table.string('image_logo', 1500)
      table.string('image_backgound', 1500)
    })
  }

  down () {
    this.drop('cenarios')
  }
}

module.exports = CenarioSchema
