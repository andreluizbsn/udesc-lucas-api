'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CenarioSchema extends Schema {
  up () {
    this.create('cenarios', (table) => {
      table.increments()
      table.string('nome', 150)
      table.string('url_image_base', 1000)      
      table.boolean('ativo').defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('cenarios')
  }
}

module.exports = CenarioSchema
