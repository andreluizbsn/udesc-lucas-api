'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EscolaSchema extends Schema {
  up () {
    this.create('escolas', (table) => {
      table.increments()
      table.string('nome', 350)
      table.string('cnpj', 14).notNullable().unique()
      table.string('sigla', 20)
      table.timestamps()
    })
  }

  down () {
    this.drop('escolas')
  }
}

module.exports = EscolaSchema
