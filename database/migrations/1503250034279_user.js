'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('nome', 280)
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('tipo', 30)
      table.integer('aluno_id')
      table.integer('professor_id')
      table.boolean('ativo').defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
