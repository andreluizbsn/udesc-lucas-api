'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DicionarioSchema extends Schema {
  up () {
    this.create('dicionarios', (table) => {
      table.increments()
      table.string('chave', 150)
      table.string('valor', 8000)
      table.timestamps()
    })
  }

  down () {
    this.drop('dicionarios')
  }
}

module.exports = DicionarioSchema
